package main

import "fmt"

func main() {
	var a, b int = 3, 4

	// fmt.Scanf("%d", &a)
	// fmt.Scanf("%d", &b)
	fmt.Printf("a = %d, b = %d\n", a, b)

	// fmt.Println(a, "is odd: ", isOdd(a))
	// fmt.Println(b, "is even: ", isEven(b))
}

func isEven(num int) bool {
	var even_number bool
	if num > 0 {
		if num % 2 == 0 {
			even_number = true
		}else {
			even_number = false
		}
	}
	return even_number
}

func isOdd(num int) bool {
	var odd_number bool
	if num > 0 {
		if num % 2 != 0 {
			odd_number = true
		}else {
			odd_number = false
		}
	}
	return odd_number
}
