package main

import (
	"fmt"
)

func main() {
	var r float32 = 10.04

	// fmt.Scanf("%f", &r)
	fmt.Println("R =", r)

	fmt.Printf("Area: %0.2f\n", area(r))
}

func area(r float32) (area float32) {
	diameter := 2 * r
	squareArea := float32(math.Pow(float64(diameter), 2))
	circleArea := float32(math.Pow(float64(r), 2) * math.Pi)
	area = squareArea - circleArea
	stringForm := fmt.Sprintf("%.2f", area) 

	value, err := strconv.ParseFloat(stringForm, 32)
	if err != nil {
		log.Fatal(err)
	}
	area = float32(value)
	return area
}
